import { createStackNavigator } from "react-navigation";
import {
  HomeScreen,
  DetailsScreen,
  CalendarScreen,
  EditScreen,
  SearchScreen
} from '../screens';

const StackNavigator = createStackNavigator(
  {
    Home: HomeScreen,
    Details: DetailsScreen,
    Edit: EditScreen,
    Search: SearchScreen,
    Calendar: CalendarScreen,
  },
  {
    initialRouteName: "Home",
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: 'steelblue',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  }
);


export default StackNavigator;