import {createDrawerNavigator} from 'react-navigation';
import StackNavigator from './stackNavigator';
import DrawerComponent from '../components/drawerComponent/drawerComponent';


export default DrawerNavigator = createDrawerNavigator(
    {
        screen: StackNavigator
    },
    {
        contentComponent: DrawerComponent,
    }
  );