import { createAppContainer, createSwitchNavigator } from "react-navigation";
import StackNavigator from "./stackNavigator";
import DrawerNavigator from "./drawerNavigator";


//export default createAppContainer(StackNavigator);
export default createAppContainer(createSwitchNavigator(
    {
        App: DrawerNavigator,
    },
    {
        initialRouteName: 'App',
    }
));
