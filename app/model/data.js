export const entries = [
    {
        title: 'My island',
        datetime: '1528101680',
        content: 'Dear Esther. I sometimes feel as if I’ve given birth to this island. Somewhere, between the longitude and latitude a split opened up and it beached remotely here. No matter how hard I correlate, it remains a singularity, an alpha point in my life that refuses all hypothesis. I return each time leaving fresh markers that I hope, in the full glare of my hopelessness, will have blossomed into fresh insight in the interim.' 
    },
    {
        title: 'Donnelly',
        datetime: '1529101770',
        content: 'Dear Esther. The gulls do not land here anymore; I’ve noticed that this year they seem to have shunned this place. Perhaps it’s the depletion of the fishing stock driving them away. Perhaps it’s me. When he first landed here, Donnelly wrote that the herds were sickly and their shepherds the lowest of the miserable classes that populate these Hebridean islands. Three hundred years later, even they have departed.' 
    },
    {
        title: 'Lost',
        datetime: '1529202130',
        content: 'Dear Esther. I have lost track of how long I have been here, and how many visits I have made overall. Certainly, the landmarks are now so familiar to me that I have to remind myself to actually see the forms and shapes in front of me. I could stumble blind across these rocks, the edges of these precipices, without fear of missing my step and plummeting down to sea. Besides, I have always considered that if one is to fall, it is critical to keep one’s eyes firmly open.' 
    },
    {
        title: 'Crush',
        datetime: '1529803240',
        content: 'Dear Esther. The morning after I was washed ashore, salt in my ears, sand in my mouth and the waves always at my ankles, I felt as though everything had conspired to this one last shipwreck. I remembered nothing but water, stones in my belly and my shoes threatening to drag me under to where only the most listless of creatures swim.' 
    },
    {
        title: "Donnelly's book",
        datetime: '1529903252',
        content: 'Donnelly’s book had not been taken out from the library since 1974. I decided it would never be missed as I slipped it under my coat and avoided the librarian’s gaze on the way out. If the subject matter is obscure, the writer’s literary style is even more so, it is not the text of a stable or trustworthy reporter. Perhaps it is fitting that my only companion in these last days should be a stolen book written by a dying man.' 
    },
    {
        title: 'Alone',
        datetime: '1530004112',
        content: 'I have become convinced I am not alone here, even though I am equally sure it is simply a delusion brought upon by circumstance. I do not, for instance, remember where I found the candles, or why I took it upon myself to light such a strange pathway. Perhaps it is only for those who are bound to follow.' 
    },

];

export const convertDate = (dateInteger)=>{
    let date = new Date(parseInt(dateInteger)*1000);
    return date.toLocaleString();
  };