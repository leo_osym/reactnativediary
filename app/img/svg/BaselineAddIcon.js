import React from "react";
import Svg, { Path } from "react-native-svg";

const BaselineAddIcon = props => (
<Svg width={props.width || 24} height={props.height || 24} viewBox="0 0 24 24"><Path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z" fill={props.color ? props.color : '#000'}></Path>
  <Path d="M0 0h24v24H0z" fill="none"></Path></Svg>
);

export default BaselineAddIcon;
