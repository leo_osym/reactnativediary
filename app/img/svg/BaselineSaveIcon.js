import React from "react";
import Svg, { Path } from "react-native-svg";

const BaselineSaveIcon = props => (
<Svg width={props.width || 24} height={props.height || 24} viewBox="0 0 24 24"><Path d="M0 0h24v24H0z" fill="none"></Path>
  <Path d="M17 3H5a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14c1.1 0 2-.9 2-2V7l-4-4zm-5 16c-1.66 0-3-1.34-3-3s1.34-3 3-3 3 1.34 3 3-1.34 3-3 3zm3-10H5V5h10v4z" fill={props.color ? props.color : '#000'}></Path></Svg>
);

export default BaselineSaveIcon;
