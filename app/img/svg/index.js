import baselineAddIcon from './BaselineAddIcon';
import baselineDeleteIcon from './BaselineDeleteIcon';
import baselineCalendarIcon from './BaselineCalendarIcon';
import baselineEditIcon from './BaselineEditIcon';
import baselineInfoIcon from './BaselineInfoIcon';
import baselineSaveIcon from './BaselineSaveIcon';
import baselineSearchIcon from './BaselineSearchIcon';
import baselineSyncIcon from './BaselineSyncIcon';
import baselineSettingsIcon from './BaselineSettingsIcon';
import hamburger from './Hamburger';
import user from './User';
import night from './Night';


export const BaselineSaveIcon = baselineSaveIcon;
export const BaselineDeleteIcon = baselineDeleteIcon;
export const BaselineAddIcon = baselineAddIcon;
export const BaselineCalendarIcon = baselineCalendarIcon;
export const BaselineEditIcon = baselineEditIcon;
export const BaselineInfoIcon = baselineInfoIcon;
export const BaselineSearchIcon = baselineSearchIcon;
export const BaselineSyncIcon = baselineSyncIcon;
export const BaselineSettingsIcon = baselineSettingsIcon;
export const Hamburger = hamburger;
export const User = user;
export const Night = night;
