import React from "react";
import Svg, { Path } from "react-native-svg";

const Hamburger = props => (
<Svg width={props.width || 459} height={props.height || 459} viewBox="0 0 459 459"><Path d="M0 382.5h459v-51H0v51zM0 255h459v-51H0v51zM0 76.5v51h459v-51H0z" fill={props.color ? props.color : '#000'}></Path></Svg>
);

export default Hamburger;
