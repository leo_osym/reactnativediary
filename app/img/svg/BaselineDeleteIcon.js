import React from "react";
import Svg, { Path } from "react-native-svg";

const BaselineDeleteIcon = props => (
<Svg width={props.width || 24} height={props.height || 24} viewBox="0 0 24 24"><Path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z" fill={props.color ? props.color : '#000'}></Path>
  <Path d="M0 0h24v24H0z" fill="none"></Path></Svg>
);

export default BaselineDeleteIcon;
