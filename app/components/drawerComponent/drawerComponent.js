import React, { Fragment } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import {
  BaselineAddIcon, BaselineEditIcon, BaselineCalendarIcon,
  BaselineInfoIcon, BaselineSearchIcon, BaselineSettingsIcon,
  BaselineSyncIcon
} from '../../img/svg';
import DrawerHeaderComponent from '../drawerHeaderComponent/drawerHeaderComponent';
import DrawerEntryComponent from '../drawerEntryComponent/drawerEntryComponent';

const DrawerComponent = () => {
  return (
    <Fragment>
        <View style={styles.content}>
          <DrawerHeaderComponent />
          <View style={{marginBottom: 10, height: 1, backgroundColor: '#44484a', width: '100%'}}/>
          <DrawerEntryComponent 
            icon={<BaselineSettingsIcon height={40} width={40} color='#44484a'/>} 
            title='Settings' 
            onEntryPress={()=>{}}/>
            <DrawerEntryComponent 
            icon={<BaselineSyncIcon height={40} width={40} color='#44484a'/>} 
            title='Sync' 
            onEntryPress={()=>{}}/>
            <DrawerEntryComponent 
            icon={<BaselineInfoIcon height={40} width={40} color='#44484a'/>} 
            title='About' 
            onEntryPress={()=>{}}/>
        </View>
    </Fragment>
  );
};

const styles = StyleSheet.create({
  content: {
    width: '100%',
    height: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#f4f9fc',
  },
  drawerItem: {
    width: '100%',
    margin: 5,
    marginLeft: 20,
    //justifyContent: 'center',
    alignItems: "center",
    flexDirection: 'row'
  },
  drawerItemText: {
    fontSize: 28,
    marginLeft: 20
  }
});

export default DrawerComponent;
