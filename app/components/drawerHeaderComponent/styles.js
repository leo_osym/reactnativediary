import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    userField: {
        height: 150,
        width: '100%',
        padding: 10,
        marginTop: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'stretch',
        //borderWidth: 2,
        //borderColor: '#000'
        backgroundColor: '#eef7fa'
    },
    nightMode: {
        justifyContent: 'center',
        alignItems: 'flex-end',
        //margin: 10,
        marginTop: 50,
        margin: 20
    },
    userView: {
        justifyContent: "flex-start",
        alignItems: 'center',
        alignSelf: 'flex-end',
        marginLeft: 10
    },
    userTitle: {
        fontSize: 20,
        color: '#000',
        //backgroundColor: '#fff'
    },
    userInfo: {
        fontSize: 15,
        color: '#000',
        //backgroundColor: '#fff'
    },
});