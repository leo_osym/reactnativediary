import React from 'react';
import { View, Text } from 'react-native';
import { User, Night } from '../../img/svg';
import { styles } from './styles';

const DrawerHeaderComponent = () => {
    return (
        <View style={styles.userField}>
            <View style={styles.userView}>
                <User width={70} height={70} />
                <Text style={styles.userTitle}>Dean Moriarty</Text>
                <Text style={styles.userInfo}>132 Entries</Text>
            </View>
            <View style={styles.nightMode}>
                <Night width={30} height={30} />
            </View>
        </View>
    );
};

export default DrawerHeaderComponent;
