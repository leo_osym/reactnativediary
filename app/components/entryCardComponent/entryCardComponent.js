import React, { Fragment } from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';

const EntryCardComponent = (props) => {

    return (
        <TouchableOpacity style={styles.main} onPress={props.onCardPress}>
            <Text style={styles.title}>{props.title}</Text>
            <Text style={styles.datetime}>{props.datetime}</Text>
            <Text style={styles.content}>{props.content}</Text>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    main: {
        width: '95%',
        height: 140,
        padding: 10,
        margin: 5,
        //justifyContent: 'center',
        //alignItems: 'center',
        backgroundColor: '#bcdfee',
        //borderColor: '#44484a',
        //borderWidth: 1,
        borderRadius: 20
    },
    title:{
        //margin: 5,
        fontSize: 26,
        fontWeight: 'bold'
    },
    datetime: {
        marginTop: 5,
        fontSize: 18,
        fontWeight: 'bold'
    },
    content: {
        marginTop: 5,
        fontSize: 20,
    }
});

export default EntryCardComponent;
