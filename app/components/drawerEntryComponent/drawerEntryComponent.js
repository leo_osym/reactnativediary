import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { styles } from './styles';

const DrawerEntryComponent = (props) => {

    onPress = () => {
        props.onEntryPress();
    }
    return (
        <TouchableOpacity onPress={onPress} style={styles.drawerItem}>
            {props.icon}
            <Text style={styles.drawerItemText}>{props.title}</Text>
        </TouchableOpacity>
    );
};

export default DrawerEntryComponent;
