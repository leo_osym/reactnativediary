import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    drawerItem: {
        width: '100%',
        margin: 5,
        marginLeft: 20,
        justifyContent: 'flex-start',
        alignItems: "center",
        flexDirection: 'row'
    },
    drawerItemText: {
        fontSize: 28,
        marginLeft: 20,
        color: '#44484a'
    }
});