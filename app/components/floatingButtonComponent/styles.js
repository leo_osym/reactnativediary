import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    floatingButton: {
        position: "absolute",
        bottom: 40,
        right: 20,
        width: 66,
        height: 66,
        borderRadius: 33,
        backgroundColor: '#ff2500',
        justifyContent: 'center',
        alignItems: 'center'
    },
  });