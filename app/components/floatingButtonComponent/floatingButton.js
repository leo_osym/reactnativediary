import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { BaselineAddIcon } from '../../img/svg';
import { styles } from './styles';

const FloatingButton = (props) => {

    return (
        <TouchableOpacity style={styles.floatingButton} onPress={props.onPress}>
            <BaselineAddIcon height={30} width={30} color={'#fff'} />
        </TouchableOpacity>
    );
};

export default FloatingButton;
