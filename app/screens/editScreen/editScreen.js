import React, { Fragment, useEffect, useState } from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  StatusBar,
  TouchableOpacity,
  TextInput
} from 'react-native';
import { BaselineSaveIcon, BaselineDeleteIcon } from '../../img/svg';
import { styles } from './styles';
import { entries, convertDate } from '../../model/data';

const EditScreen = (props) => {
  const [index, setIndex] = useState(0);
  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');

  useEffect(() => {
    let ind = props.navigation.getParam('index');
    if(ind){
      let index = parseInt(ind) ? parseInt(ind) : 0;
      setIndex(index);
      setTitle(entries[index].title);
      setContent(entries[index].content);
    }
  }, []); // use only once

  return (
    <Fragment>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView>
          <View style={styles.main}>
            <TextInput value={title} placeholder='Title' style={styles.title} onChangeText={(text) => setTitle(text)}/>
            <TextInput value={content} placeholder='Content' multiline={true} style={styles.content} onChangeText={(text) => setContent(text)}/>
          </View>
        </ScrollView>
      </SafeAreaView>
    </Fragment>
  );
};

EditScreen.navigationOptions = props => (
  {
    headerTitle: 'Edit',
    headerRight: (
      <View style={styles.headerRightIconBar}>
        <TouchableOpacity onPress={() => { }}>
          <BaselineSaveIcon width={30} height={30} color={'#fff'} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => { }}>
          <BaselineDeleteIcon width={30} height={30} color={'#fff'} />
        </TouchableOpacity>
      </View>
    ),
  }
);

export default EditScreen;
