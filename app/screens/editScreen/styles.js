import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    main: {
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        margin: 10,
    },
    title: {
        width: '95%',
        //flex: 1,
        fontSize: 30,
        fontWeight: 'bold',
        //borderBottomColor: 'red',
        //borderBottomWidth: 1
    },
    content: {
        width: '95%',
        marginTop: 10,
        fontSize: 25,
        //borderBottomColor: 'red',
        //borderBottomWidth: 1
    },
    headerLeftIconBar: {
        marginLeft: 10
    },
    headerRightIconBar: {
        width: 80,
        //borderWidth: 2, 
        //borderColor: 'black', 
        flexDirection: 'row',
        justifyContent: 'space-around'
    }
});