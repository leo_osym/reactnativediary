import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
    main: {
      padding: 10,
      width: '100%',
      height: '100%',
      //justifyContent: 'flex-start',
      //alignItems: 'center',
      backgroundColor: '#fff'
    },
    title: {
      fontSize: 30,
      fontWeight: 'bold',
      marginBottom: 10,
    },
    datetime: {
      fontSize: 25,
      fontWeight: 'bold',
      marginBottom: 10,
    },
    content: {
      fontSize: 24,
      marginBottom: 10,
    },
    headerLeftIconBar: {
      marginLeft: 10
    },
    headerRightIconBar: {
      width: 80,
      //borderWidth: 2, 
      //borderColor: 'black', 
      flexDirection: 'row',
      justifyContent: 'space-around'
    }
  });