import React, { Fragment, useEffect, useState } from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity
} from 'react-native';
import { BaselineEditIcon, BaselineSearchIcon } from '../../img/svg';
import { entries, convertDate } from '../../model/data';
import { styles } from './styles';

const DetailsScreen = (props) => {
  const [index, setIndex] = useState(0);

  useEffect(() => {
    let ind = parseInt(props.navigation.getParam('index', '0'));
    setIndex(ind);
  });

  return (
    <Fragment>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView>
          <View style={styles.main}>
            <Text style={styles.title}>{entries[index].title}</Text>
            <Text style={styles.datetime}>{convertDate(entries[index].datetime)}</Text>
            <Text style={styles.content}>{entries[index].content}</Text>
          </View>
        </ScrollView>
      </SafeAreaView>
    </Fragment>
  );
};

parseIndex = (props)=>{
  return ind = parseInt(props.navigation.getParam('index', '0'));
}

DetailsScreen.navigationOptions = props => (
  {
    headerTitle: 'Details',
    headerRight: (
      <View style={styles.headerRightIconBar}>
        <TouchableOpacity onPress={() => { props.navigation.navigate('Edit', {index: parseIndex(props)}) }}>
          <BaselineEditIcon width={30} height={30} color={'#fff'} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => { }}>
          <BaselineSearchIcon width={30} height={30} color={'#fff'} />
        </TouchableOpacity>
      </View>
    ),
  }
);

export default DetailsScreen;
