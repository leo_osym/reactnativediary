import React, { Fragment } from 'react';
import {
  SafeAreaView,
  FlatList,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  TouchableOpacity
} from 'react-native';
import {
  BaselineAddIcon, BaselineEditIcon, BaselineCalendarIcon,
  BaselineInfoIcon, BaselineSearchIcon, BaselineSettingsIcon,
  BaselineSyncIcon, Hamburger, User, Night
} from '../../img/svg';
import { styles } from './styles';
import EntryCardComponent from '../../components/entryCardComponent/entryCardComponent';
import {entries, convertDate} from '../../model/data';
import FloatingButton from '../../components/floatingButtonComponent/floatingButton';

const HomeScreen = (props) => {

  goDetails = () => {
    props.navigation.navigate('Details');
  }
  goEdit = () => {
    props.navigation.navigate('Edit');
  }
  goCalendar = () => {
    props.navigation.navigate('Calendar');
  }
  goSearch = () => {
    props.navigation.navigate('Search');
  }
  openDrawer = () => {
    console.log('works');
    props.navigation.openDrawer();
  }

  sliceContent = (content)=>{
    let newContent = content.slice(0, 70);
    let ind = newContent.match(/\s([^\s]+)$/);
    if(!ind) return newContent+'...'
    return newContent.slice(0,ind.index)+'...';
  }

  showDetails=(index)=>{
    props.navigation.navigate('Details', {index: index});
  }

  return (
    <Fragment>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.main}>
        <ScrollView>
        <View style={styles.content}>
          {
            entries.map((item, index) => 
              <EntryCardComponent title={item.title} datetime={convertDate(item.datetime)} content={sliceContent(item.content)} onCardPress={()=>{showDetails(index)}}/>
            )
          }
        </View>
        </ScrollView>
        <FloatingButton onPress={() => { props.navigation.navigate('Edit', {entry : null}) }}/>
      </SafeAreaView>
    </Fragment>
  );
};

HomeScreen.navigationOptions = props => (
  {
    headerLeft: (
      <View style={styles.headerLeftIconBar}>
        <TouchableOpacity onPress={() => { props.navigation.openDrawer() }}>
          <Hamburger width={30} height={30} color={'#fff'}/>
        </TouchableOpacity>
      </View>
    ),
    headerRight: (
      <View style={styles.headerRightIconBar}>
        <TouchableOpacity onPress={() => { props.navigation.openDrawer() }}>
          <BaselineSyncIcon width={30} height={30} color={'#fff'}/>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => { props.navigation.navigate('Calendar') }}>
          <BaselineCalendarIcon width={30} height={30} color={'#fff'}/>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => { props.navigation.navigate('Search') }}>
          <BaselineSearchIcon width={30} height={30} color={'#fff'}/>
        </TouchableOpacity>
      </View>
    ),
  }
);

export default HomeScreen;
