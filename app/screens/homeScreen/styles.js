import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    main:{
        backgroundColor: '#e4f2f8'
    },
    content:{
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#e4f2f8'
    },
    welcome: {
        fontSize: 40,
        color: '#000',
        backgroundColor: '#fff'
    },
    floatingButton: {
        position: "absolute",
        bottom: 40,
        right: 20,
        width: 66,
        height: 66,
        borderRadius: 33,
        backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerLeftIconBar: {
        marginLeft: 10 
    },
    headerRightIconBar: {
        width: 140, 
        //borderWidth: 2, 
        //borderColor: 'black', 
        flexDirection:'row', 
        justifyContent: 'space-around' 
    }
  });