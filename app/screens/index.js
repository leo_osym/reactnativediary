import homeScreen from './homeScreen/homeScreen';
import editScreen from './editScreen/editScreen';
import detailsScreen from './detailsScreen/detailsScreen';
import calendarScreen from './calendarScreen/calendarScreen';
import searchScreen from './searchScreen/searchScreen';

export const HomeScreen = homeScreen;
export const EditScreen = editScreen;
export const DetailsScreen = detailsScreen;
export const CalendarScreen = calendarScreen;
export const SearchScreen = searchScreen;