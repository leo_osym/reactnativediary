import React, {Fragment} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput
} from 'react-native';

const SearchScreen = () => {
  return (
    <Fragment>
      <StatusBar barStyle='dark-content' />
      <SafeAreaView>
        <View style={styles.content}>
            <Text style={styles.welcome}>SearchScreen</Text>
        </View>
      </SafeAreaView>
    </Fragment>
  );
};

SearchScreen.navigationOptions = props => (
  {
    headerTitle: (
      <View style={styles.headerLeftIconBar}>
        <TextInput placeholder='Search' style={styles.searchField}/>
      </View>
    ),
  }
);

const styles = StyleSheet.create({
  content:{
      width: '100%',
      height: '100%',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#fff'
  },
  welcome: {
      fontSize: 40,
      color: '#000',
      backgroundColor: '#fff'
  },
  searchField: {
    width: 200,
    fontSize: 22,
    borderBottomWidth: 2, 
    borderBottomColor: 'red'}
});

export default SearchScreen;
