import React from 'react';
import AppContainer from './navigation/appContainer';
console.disableYellowBox = true;

const App = () => {
  return (
    <AppContainer/>
  );
};

export default App;
